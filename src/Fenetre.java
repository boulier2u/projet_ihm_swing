import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Fenetre extends JFrame {
	
	DessinFigures dessin;
	
	

	
	public Fenetre(String nomFenetre){
		super(nomFenetre);
		 int hauteur, largeur;
		
			Dimension tailleEcran = java.awt.Toolkit.getDefaultToolkit()
				.getScreenSize();
			hauteur = (int) tailleEcran.getHeight();
			largeur = (int) tailleEcran.getWidth();
			
	
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			dessin = new DessinFigures();
			JPanel contenant = new JPanel();
			contenant.setLayout(new BorderLayout());
			contenant.setPreferredSize(new Dimension(largeur,hauteur));	
			JPanel choix = new PanneauChoix(dessin);
			contenant.add(choix, BorderLayout.NORTH);
			contenant.add(dessin, BorderLayout.CENTER);
			this.setContentPane(contenant);
			pack();
			
		
			
			 
			// On prévient notre JFrame que notre JPanel sera son content pane
			 
			this.setVisible(true);
			

	}

	public static void main(String[] args) {
		
		Fenetre fen = new Fenetre("Ma fenetre");
		
		
		
	}
}