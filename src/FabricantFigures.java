import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FabricantFigures implements MouseListener {
	private FigureColoree figure_en_cours_de_fabrication;
	private int nb_points_cliques;
	private Point[] points_cliques;

	public FabricantFigures(FigureColoree f) {
		this.figure_en_cours_de_fabrication = f;
		this.nb_points_cliques = 0;
		this.points_cliques = new Point[f.nbPoints()];

	}

	public void mouseEntered(MouseEvent m) {

	}

	public void mouseExited(MouseEvent n) {

	}

	public void mouseReleased(MouseEvent o) {

	}

	public void mouseClicked(MouseEvent p) {

	}

	public void mousePressed(MouseEvent q) {

		if (nb_points_cliques < points_cliques.length - 1) {
			points_cliques[nb_points_cliques] = new Point(q.getX(), q.getY());
			nb_points_cliques++;
		} else {
			points_cliques[nb_points_cliques] = new Point(q.getX(), q.getY());
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			
			((DessinFigures) (q.getSource()))
					.ajoute(figure_en_cours_de_fabrication);
			((Component) q.getSource()).removeMouseListener(this);
		}
	}

}
