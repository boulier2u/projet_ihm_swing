import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

//PAANNNNNNNEAU
public class PanneauChoix extends JPanel {
	private FigureColoree fc;
	private DessinFigures df;

	public PanneauChoix(DessinFigures dessin) {
		this.df = dessin;
		this.setLayout(new BorderLayout());

		// Groupe Boutton Radio
		final JRadioButton nouvelleFigure = new JRadioButton("Nouvelle figure");
		final JRadioButton trace = new JRadioButton("Trace a main levee");
		final JRadioButton manip = new JRadioButton("Manipulations");
		ButtonGroup bg = new ButtonGroup();
		bg.add(nouvelleFigure);
		bg.add(trace);
		bg.add(manip);

		JPanel haut = new JPanel();
		haut.add(nouvelleFigure);
		haut.add(trace);
		haut.add(manip);
		add(haut, BorderLayout.NORTH);

		final JComboBox<String> choixForme = new JComboBox<String>(
				new String[] { "Quadrilatere", "Rectangle", "Triangle", "Carre", "Losange", "Cercle" });
		choixForme.setEnabled(false);

		final JComboBox<String> choixCouleur = new JComboBox<String>(
				new String[] { "Rouge", "Bleu", "Vert", "ORANGEADE" });

		choixCouleur.setEnabled(false);

		final JButton effacer = new JButton("Effacer");

		JPanel bas = new JPanel();
		bas.add(choixForme);
		bas.add(choixCouleur);
		bas.add(effacer);
		add(bas, BorderLayout.SOUTH);

		effacer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (effacer.isSelected()) {
					df.clear();

				}
			}
		});

		nouvelleFigure.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (nouvelleFigure.isSelected()) {
					choixForme.setEnabled(true);
					choixCouleur.setEnabled(true);

				}
			}
		});

		choixForme.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				fc = creeFigure(choixForme.getSelectedIndex());
				df.construit(fc);
			}

		});

		choixCouleur.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int j = choixCouleur.getSelectedIndex();
				System.out.println("valeur de  j :" + j);
				fc.setCouleur(determineCouleur(j));
				System.out.println(fc.getCouleur() + "");
				df.construit(fc);
			}
		});

		trace.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (trace.isSelected()) {
					choixForme.setEnabled(false);
					choixCouleur.setEnabled(true);
				}

			}
		});

	}

	public Color determineCouleur(int j) {
		Color c = null;
		switch (j) {

		case 0:
			c = Color.RED;
			System.out.println("Je change de Couleur");
			break;
		case 1:
			c = Color.BLUE;
			break;
		case 2:
			c = Color.GREEN;
			break;
		case 3:
			c = Color.ORANGE;
			break;
		default:
			break;
		}
		return c;

	}

	public FigureColoree creeFigure(int i) {
		FigureColoree f = null;
		switch (i) {
		case 0:
			f = new Quadrilatere();
			break;
		case 1:
			f = new Rectangle();
			break;
		case 2:
			f = new Triangle();
			break;
		case 3:
			f = new Carre();
			break;
		default:
			break;
		}
		return f;

	}
}
