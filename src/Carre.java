public class Carre extends Quadrilatere {

	public Carre() {
		super();
	}

	@Override
	public int nbPoints() {
		return 4;
	}

	public void modifierPoints(Point[] p) {
		tab_mem[0] = p[0];
		tab_mem[1] = p[1];
		tab_mem[2] = new Point(tab_mem[1].rendreY() - tab_mem[0].rendreY()
				+ tab_mem[1].rendreX(), (tab_mem[0].rendreX())
				- tab_mem[1].rendreX() + tab_mem[1].rendreY());
		tab_mem[3] = new Point(tab_mem[1].rendreY() - tab_mem[0].rendreY()
				+ tab_mem[0].rendreX(), (tab_mem[0].rendreX())
				- tab_mem[1].rendreX() + tab_mem[0].rendreY());

	}

}
