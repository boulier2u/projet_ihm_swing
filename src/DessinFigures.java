import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class DessinFigures extends JPanel {

	private ArrayList<FigureColoree> lfg;

	public DessinFigures() {
		this.lfg = new ArrayList<FigureColoree>();

	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		if (lfg != null) {
			for (int i = 0; i < lfg.size(); i++) {
				g.setColor(this.lfg.get(i).getCouleur());
				this.lfg.get(i).affiche(g);

			}
		}

	}

	public void ajoute(FigureColoree f) {
		if (f != null) {
			this.lfg.add(f);
			f.selectionne();
		}
		this.repaint();
	}

	public void construit(FigureColoree f) {
		FabricantFigures f1 = new FabricantFigures(f);
		this.addMouseListener(f1);

	}

	public void clear() {
		this.lfg = new ArrayList<FigureColoree>();
		removeAll();
		repaint();
	}
}