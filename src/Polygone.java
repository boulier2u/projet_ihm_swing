import java.awt.Graphics;
import java.awt.Polygon;

public abstract class Polygone extends FigureColoree {
	private Polygon p;

	public Polygone() {
		super();
	}

	public void affiche(Graphics g) {
		super.affiche(g);
		int[] tabX = new int[this.nbPoints()];
		int[] tabY = new int[this.nbPoints()];
		for (int i = 0; i < this.nbPoints(); i++) {
			tabX[i] = this.tab_mem[i].rendreX();
			tabY[i] = this.tab_mem[i].rendreY();
		}
		this.p = new Polygon(tabX, tabY, this.nbPoints());

		g.fillPolygon(p);

	}

	public int nbClics() {
		return this.nbPoints();
	}

	public void modifierPoints(Point[] p1) {
		this.tab_mem = p1;

	}

	public boolean estDedans(int x, int y) {
		int i;
		int j;
		boolean result = false;
		for (i = 0, j = this.tab_mem.length - 1; i < this.tab_mem.length; j = i++) {
			if ((this.tab_mem[i].rendreY() > y) != (this.tab_mem[j].rendreY() > y)
					&& (x < (this.tab_mem[j].rendreX() - this.tab_mem[i]
							.rendreX())
							* (y - this.tab_mem[i].rendreY())
							/ (this.tab_mem[j].rendreY() - tab_mem[i].rendreY())
							+ this.tab_mem[i].rendreX())) {
				result = !result;
			}
		}
		return result;

	}
}
