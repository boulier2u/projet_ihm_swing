import java.awt.Color;
import java.awt.Graphics;

public abstract class FigureColoree {
	private final static int TAILLE_CARRE_SELECTION = 4;
	private final static int PERIPHERIE_CARRE_SELECTION = 8;
	private boolean selected;
	private Color couleur;
	protected Point[] tab_mem;

	public FigureColoree() {
		this.selected = false;
		this.couleur = Color.RED;
		this.tab_mem = new Point[this.nbPoints()];
	}

	public abstract int nbPoints();

	public abstract int nbClics();

	public abstract boolean estDedans(int i, int j);

	public abstract void modifierPoints(Point[] p);

	
	public void affiche(Graphics g) {
		if (selected) {

			for (int i = 0; i < tab_mem.length - 1; i++) {
				 g.drawRect(tab_mem[i].rendreX(), tab_mem[i].rendreY(), TAILLE_CARRE_SELECTION, TAILLE_CARRE_SELECTION);

				}
			}else{
				this.affiche(g);
			}

		}
	

	public void translation(int dx, int dy) {
		for (int i = 0; i < tab_mem.length; i++) {
			this.tab_mem[i].translation(dx, dy);
		}

	}

	public int carreDeSelection(int x, int y) {
		int ind = -1;
		for (int i = 0; i < tab_mem.length; i++) {
			if (Math.abs(tab_mem[i].rendreX() - x) <= this.PERIPHERIE_CARRE_SELECTION) {
				if ((Math.abs(tab_mem[i].rendreY() - y)) <= this.PERIPHERIE_CARRE_SELECTION) {
					ind = i;
					this.selectionne();

				} else {
					this.deSelectionne();

				}
			}
			i++;

		}
		return ind;

	}

	public void selectionne() {
		this.selected = true;

	}

	public void deSelectionne() {
		this.selected = false;
	}

	public void changeCouleur(Color c) {
		this.setCouleur(c);
	}

	public Color getCouleur() {
		return couleur;
	}

	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
}