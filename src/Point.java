public class Point {
	private int x;
	private int y;

	public Point(int i, int j) {
		this.x =i;
		this.y =j;
	}

	public double distance(Point p) {
		double distance = Math.sqrt((p.rendreX()-this.x)*(p.rendreX()-this.x) + (p.rendreY()-this.x)*(p.rendreY()-this.y));
		return distance;

	}

	public int rendreX() {
		return x;

	}

	public int rendreY() {
		return y;

	}

	public void incrementerX(int k) {
		this.x+=k;
	}

	public void incrementerY(int l) {
		this.y+=l;
	}

	public void modifierX(int m) {
		this.x = m;
	}

	public void modifierY(int n) {
		this.y = n;
	}

	public void translation(int dx, int dy) {
		this.x += dx;
		this.y += dy;
	}
}
